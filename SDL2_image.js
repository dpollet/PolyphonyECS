'use strict'

const SDL = require('./SDL2')
const ffi = require('ffi')

exports.SDL_IMAGE_MAJOR_VERSION = 2
exports.SDL_IMAGE_MINOR_VERSION = 0
exports.SDL_IMAGE_PATCHLEVEL = 4
exports.SDL_IMAGE_COMPILEDVERSION = 2004
exports.IMG_INIT_JPG = 0x01
exports.IMG_INIT_PNG = 0x02
exports.IMG_INIT_TIF = 0x04
exports.IMG_INIT_WEBP = 0x08

exports.SDL_IMAGE_VERSION = (X) => {
	X.major = exports.SDL_IMAGE_MAJOR_VERSION
	X.minor = exports.SDL_IMAGE_MINOR_VERSION
	X.patch = exports.SDL_IMAGE_PATCHLEVEL
}

exports.SDL_IMAGE_VERSION_ATLEAST = (X, Y, Z) => exports.SDL_IMAGE_COMPILEDVERSION >= X * 1000 + Y * 100 + Z

ffi.Library(process.platform == 'win32' ? 'SDL2_image' : 'libSDL2_image', {
	IMG_Linked_Version: [ SDL.SDL_versionPtr, [ ] ],
	IMG_Init: [ 'int', [ 'int' ] ],
	IMG_Quit: [ 'void', [ ] ],
	IMG_LoadTyped_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr, 'int', 'string' ] ],
	IMG_Load: [ SDL.SDL_SurfacePtr, [ 'string' ] ],
	IMG_Load_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr, 'int' ] ],
	IMG_LoadTexture: [ 'void*', [ 'void*', 'string' ] ],
	IMG_LoadTexture_RW: [ 'void*', [ 'void*', SDL.SDL_RWopsPtr, 'int' ] ],
	IMG_LoadTextureTyped_RW: [ 'void*', [ 'void*', SDL.SDL_RWopsPtr, 'int', 'string' ] ],
	IMG_isICO: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isCUR: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isBMP: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isGIF: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isJPG: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isLBM: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isPCX: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isPNG: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isPNM: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isSVG: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isTIF: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isXCF: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isXPM: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isXV: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_isWEBP: [ 'int', [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadICO_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadCUR_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadBMP_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadGIF_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadJPG_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadLBM_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadPCX_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadPNG_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadPNM_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadSVG_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadTGA_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadTIF_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadXCF_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadXPM_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadXV_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_LoadWEBP_RW: [ SDL.SDL_SurfacePtr, [ SDL.SDL_RWopsPtr ] ],
	IMG_ReadXPMFromArray: [ SDL.SDL_SurfacePtr, [ 'char**' ] ],
	IMG_SavePNG: [ 'int', [ SDL.SDL_SurfacePtr, 'string' ] ],
	IMG_SavePNG_RW: [ 'int', [ SDL.SDL_SurfacePtr, SDL.SDL_RWopsPtr, 'int' ] ],
	IMG_SaveJPG: [ 'int', [ SDL.SDL_SurfacePtr, 'string', 'int' ] ],
	IMG_SaveJPG_RW: [ 'int', [ SDL.SDL_SurfacePtr, SDL.SDL_RWopsPtr, 'int', 'int' ] ],
}, exports)

exports.IMG_SetError = SDL.SDL_SetError
exports.IMG_GetError = SDL.SDL_GetError

/**
 * Example vector painter application using Polyphony
 * Allows drawing basic shapes and manipulating them
 * 
 * Date: 21 Feb 2019
 */
'use strict'
const fs = require('fs')
Object.assign(global, require('./Polyphony'))



/**
 * Interface
 */
let y = 2
let resetButton = Button(new Image('icons/reset.png'), 2, y)
let saveButton = Button(new Image('icons/save.png'), 2, y += 34)
let moveButton = Button(new Image('icons/move.png'), 2, y += 34, {toggleGroup: 1, toggleStyle: {backgroundColor: SDL_rgba(0, 191, 255)}})
let rectButton = Button(new Image('icons/rect.png'), 2, y += 34, {toggleGroup: 1, toggleStyle: {backgroundColor: SDL_rgba(0, 191, 255)}})
let ovalButton = Button(new Image('icons/oval.png'), 2, y += 34, {toggleGroup: 1, toggleStyle: {backgroundColor: SDL_rgba(0, 191, 255)}})
let canvas = Canvas(36, 2, 602, 476)
MetaSystem(DISPLAY_OUTPUT)



/**
 * Reset - deletes all Entities from the canvas
 */
let ResetSystem = Entity(function() {
	if (resetButton.tmpClickBy) {
		for (let c of canvas.children)
			c.delete()
		canvas.children = []
	}
}, { runOn: POINTER_INPUT, order: 60 })



/**
 * Save - serializes all Recordable Entities into SVG
 * NodeJS cannot open file dialogs, so it is saved in snapshot.svg
 */
let SaveSystem = Entity(function() {
	if (saveButton.tmpClickBy || Keyboards[0].tmpShortcut === SDLK_s) {
		let svg = '<svg xmlns="http://www.w3.org/2000/svg">'
		for (let r of canvas.children) {
			let b = r.bounds
			if (r.shape === SHAPE_RECTANGLE)
				svg += `<rect x="${b.x}" y="${b.y}" width="${b.w}" height="${b.h}"`
			else if (r.shape === SHAPE_ELLIPSE)
				svg += `<ellipse cx="${b.x + b.w / 2}" cy="${b.y + b.h / 2}" rx="${b.w / 2}" ry="${b.h / 2}"`
			let bg = r.background
			let bd = r.border
			if (bg)
				svg += ` fill="rgb(${bg.r},${bg.g},${bg.g})"`
			if (bd)
				svg += ` stroke="rgb(${bd.r},${bd.g},${bd.g})"`
			svg += '/>'
		}
		svg += '</svg>'
		fs.writeFile("snapshot.svg", svg, err => {
			console.log('Saved in snapshot.svg')
		})
	}
}, { runOn: POINTER_INPUT | KEYBOARD_INPUT, order: 61 })



/**
 * Move - relies on PointerDragSystem to (i) move shapes on the canvas, and
 *   (ii) swap shapes when dragged on side buttons
 */
let MoveTool = Entity(function() {
	if (moveButton.tmpClickBy)
		delete PointerDragSystem.disabled
	if (!moveButton.toggled)
		return
	for (let p of Pointers) {
		let d = p.tmpDropped
		let t = p.target
		if (!d)
			continue
		let db = d.bounds
		let cb = canvas.bounds
		if (t === resetButton) {
			canvas.children.splice(canvas.children.indexOf(d), 1)
			d.delete()
		} else if (t === rectButton) {
			d.shape = SHAPE_RECTANGLE
			d.bounds = db.setX(p.pressPosition.x).setY(p.pressPosition.y)
		} else if (t === ovalButton) {
			d.shape = SHAPE_ELLIPSE
			d.bounds = db.setX(p.pressPosition.x).setY(p.pressPosition.y)
		} else if (db.x < cb.x || db.x + db.w >= cb.x + cb.w || db.y < cb.y || db.y + db.h >= cb.y + cb.h) {
			d.bounds = db.setX(p.pressPosition.x).setY(p.pressPosition.y)
		}
	}
}, { runOn: POINTER_INPUT, order: 62 })



/**
 * Draw - Allows creating new shapes by press&drag, while constraining the
 *   mouse cursor inside the canvas
 */
let DrawTool = Entity(function DrawTool() {
	if (rectButton.tmpClickBy || ovalButton.tmpClickBy)
		PointerDragSystem.disabled = true
	if (!rectButton.toggled && !ovalButton.toggled)
		return
	for (let p of Pointers) {
		let d = p.drawing
		let pr = p.pressPosition
		let pos = p.cursorPosition
		let cb = canvas.bounds
		if (!d && p.buttons & 1 && p.tmpMotion && pr.x >= cb.x && pr.x < cb.x + cb.w && pr.y >= cb.y && pr.y < cb.y + cb.h) {
			p.drawing = d = Shape(rectButton.toggled ? SHAPE_RECTANGLE : SHAPE_ELLIPSE, pos.x - pr.x, pos.y - pr.y)
			d.depth = -1000
			canvas.children.push(d)
		}
		if (d) {
			if (pos.x < cb.x || pos.x >= cb.x + cb.w || pos.y < cb.y || pos.y >= cb.y + cb.h)
				pos.setX(Math.min(Math.max(pos.x, cb.x), cb.x + cb.w)).setY(Math.min(Math.max(pos.y, cb.y), cb.y + cb.h))
			d.bounds = d.bounds.setX(Math.min(pr.x, pos.x)).setY(Math.min(pr.y, pos.y))
				.setW(Math.abs(pr.x - pos.x)).setH(Math.abs(pr.y - pos.y))
			if (p.tmpReleased === 0)
				delete p.drawing
		}
	}
}, { runOn: POINTER_INPUT, order: 63 })

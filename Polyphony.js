// TODO: Intégrer le support des vues à l'affichage
// TODO: Les Composants temporaires ne doivent pas notifier les sélections
// TODO: Remplacer text par richText
// TODO: Revenir à SHAPE_RECTANGLE et SHAPE_ELLIPSE
// TODO: Bug lorsqu'une Entité est modifiée et supprimée en même temps

/* TODO v2 :
 * - Regrouper buttons, pressPosition et pressTarget dans un même Composant
 * - Intégrer le support du touch
 * - Regrouper les Composants relatifs à toggle en un seul Composant
 * - Remplacer buttons par pointerButtons
 * - Regrouper depth, bounds et shape dans un même Composant
 * - Intégrer targetable dans le composant bounds car il ne nécessite pas de Sélection
 * - Intégrer textEditable et focusStyle dans richText
 * - Ajouter un support des fenêtres avec un attribut window propagé aux children
 * _ Pour le dessin -> OpaqueRender, TextRender, AlphaRender
 */

/**
 * Vector drawing application built using Polyphony
 *
 * Author: Thibault Raffaillac <thibault.raffaillac@inria.fr>
 */
'use strict'
const fs = require('fs')
const ref = require('ref')
Object.assign(global, require('./SDL2')) // SDL symbols already carry a prefix
Object.assign(global, require('./SDL2_image'))
Object.assign(global, require('./SDL2_ttf'))
const GFX = require('./SDL2_gfx')



/**
 * Static storage
 */
const entities = new Set()
const updated = new Set() // we make no difference between Components added and modified
const deleted = new Set()
const selections = []



/**
 * Entity class - implemented as a Proxy object intercepting accesses to a
 *   native object or function
 */
let Entity_handler = {
	set(dict, prop, value, entity) {
		if (typeof value === "function")
			throw "Functions may not be used inside Components"
		updated.add(entity)
		dict[prop] = value
		return true
	},
	deleteProperty(dict, prop) {
		updated.add(dict.self)
		delete dict[prop]
		return true
	},
	// System call - update Selections now while they are not being iterated
	apply(sys, thisArg, argumentsList) {
		for (let s of selections) {
			for (let e of updated) {
				if (s.accept(e))
					s.add(e)
				else
					s.delete(e)
			}
			for (let e of deleted)
				s.delete(e)
		}
		updated.clear()
		deleted.clear()
		Reflect.apply(sys, thisArg, argumentsList)
	},
}
let Entity = exports.Entity = function(dict = {}, ext) {
	// is the argument already an Entity?
	if (entities.has(dict))
		return dict
	
	// when dict is a function, ext can contain its properties
	if (ext !== undefined)
		Object.assign(dict, ext)
	
	// enclose the object in a Proxy
	let entity = new Proxy(dict, Entity_handler)
	entities.add(entity)
	updated.add(entity)
	
	// add private fields
	Object.defineProperty(dict, 'self', { value: entity })
	Object.defineProperty(dict, 'delete', { value: function() {
		entities.delete(this.self)
		deleted.add(this.self)
	}})
	return entity
}
Entity[Symbol.iterator] = function() {
	return entities[Symbol.iterator]()
}



/**
 * Selection
 */
let Selection = exports.Selection = function(descriptor, compareFunction) {
	Array.call(this)
	this.accept = descriptor
	this.compare = compareFunction
	this.dirty = true
	this.updated = new Set()
	for (let e of entities)
		this.add(e)
	selections.push(this)
}
Selection.prototype = {
	add(e) {
		this.updated.add(e)
		if (!this.includes(e))
			this.push(e)
		this.dirty = true
		return true
	},
	delete(e) {
		this.updated.delete(e)
		let i = this.indexOf(e)
		if (i < 0)
			return false
		this[i] = this[--this.length]
		delete this[this.length]
		this.dirty = true
		return true
	},
	[Symbol.iterator]() {
		this.updated.clear()
		if (this.dirty && this.compare)
			this.sort(this.compare)
		this.dirty = false
		return super[Symbol.iterator]()
	},
}
Object.setPrototypeOf(Selection.prototype, Array.prototype)



/**
 * Selections used inside default Systems, and exported to user applications
 * Due to the overhead of updating Selections, we try to avoid duplicates
 */
let Backgrounds = new Selection(e => 'backgroundColor' in e && 'depth' in e && 'bounds' in e && 'shape' in e, (a, b) => a.depth - b.depth)
let Drawables = new Selection(e => !('hidden' in e) && 'depth' in e && 'bounds' in e && ('shape' in e || 'image' in e || 'richText' in e), (a, b) => b.depth - a.depth)
let Slides = new Selection(e => 'slide' in e)
let Serializables = new Selection(e => 'serializable' in e)
let Systems = new Selection(e => 'runOn' in e && 'order' in e, (a, b) => a.order - b.order)
let Targetables = new Selection(e => e.targetable && 'depth' in e && 'bounds' in e, (a, b) => a.depth - b.depth)
let Togglables = new Selection(e => 'toggleGroup' in e && 'toggleStyle' in e)
let TreeNodes = new Selection(e => 'children' in e)
let Views = new Selection(e => 'origin' in e && 'bounds' in e && 'depth' in e)
let Keyboards = exports.Keyboards = new Selection(e => 'keyStates' in e && 'focus' in e)
let Pointers = exports.Pointers = new Selection(e => 'cursorPosition' in e)



/**
 * Meta System - System responsible for calling other Systems
 * Defined itself as an Entity to have its Systems Selection be updated on time
 */
let POINTER_INPUT = exports.POINTER_INPUT = 1
let KEYBOARD_INPUT = exports.KEYBOARD_INPUT = 2
let DISPLAY_OUTPUT = exports.DISPLAY_OUTPUT = 4
let ALL_EVENTS = exports.ALL_EVENTS = 7
let MetaSystem = exports.MetaSystem = Entity(function MetaSystem(eventType) {
	// new Systems might appear during iteration, so make a copy first
	for (let s of Array.from(Systems)) {
		if (s.runOn & eventType && !s.disabled)
			s(eventType)
	}
})



/**
 * Utility classes
 */
let Coordinates = exports.Coordinates = function Coordinates(x = 0, y = 0, z = 0) {
	this.x = x
	this.y = y
	this.z = z
}
Coordinates.prototype.distance = function(c) { return Math.hypot(this.x - c.x, this.y - c.y, this.z - c.z) }
Coordinates.prototype.setX = function(x) { this.x = x; return this }
Coordinates.prototype.setY = function(y) { this.y = y; return this }
let Bounds = exports.Bounds = function Bounds(x, y, w, h) {
	this.x = x
	this.y = y
	this.w = w
	this.h = h
}
Bounds.prototype.setX = function(x) { this.x = x; return this }
Bounds.prototype.setY = function(y) { this.y = y; return this }
Bounds.prototype.setW = function(w) { this.w = w; return this }
Bounds.prototype.setH = function(h) { this.h = h; return this }
exports.Line = function Line(x1, y1, x2, y2, arrow1 = false, arrow2 = false) {
	this.x1 = x1
	this.y1 = y1
	this.x2 = x2
	this.y2 = y2
	this.arrow1 = arrow1
	this.arrow2 = arrow2
}
exports.Border = function Border(color, w = 1) {
	this.color = color
	this.w = w
}
exports.Image = function Image(src, opacity = 1, paddingW = 0, paddingH = 0) {
	this.src = src
	this.opacity = opacity
	this.paddingW = paddingW
	this.paddingH = paddingH
	let s = IMG_Load(src)
	Object.defineProperty(this, 'w', {value: s.deref().w})
	Object.defineProperty(this, 'h', {value: s.deref().h})
	Object.defineProperty(this, 'dstrect', {value: SDL_Rect()})
	Object.defineProperty(this, 'texture', {value: SDL_CreateTextureFromSurface(renderer, s)})
	if (opacity !== 1) {
		SDL_SetTextureAlphaMod(this.texture, 255 * opacity)
		SDL_SetTextureBlendMode(this.texture, SDL_BLENDMODE_BLEND)
	}
	SDL_FreeSurface(s);
}
const advancePtr = ref.alloc('int')
let Glyph = exports.Glyph = function Glyph(font, code) {
	this.font = font
	this.code = code
	TTF_GlyphMetrics(font.ttf, code, null, null, null, null, advancePtr)
	Object.defineProperty(this, 'advance', {value: advancePtr.deref()})
	Object.defineProperty(this, 'surface', {value: TTF_RenderGlyph_Blended(font.ttf, code, font.color)})
}
let Font = exports.Font = function Font(file, size, style = 0, color = SDL_rgba(0, 0, 0)) {
	this.file = file
	this.size = size
	this.style = style
	this.color = color
	Object.defineProperty(this, 'ttf', {value: TTF_OpenFont(file, size)})
	TTF_SetFontStyle(this.ttf, style)
	Object.defineProperty(this, 'height', {value: TTF_FontHeight(this.ttf)})
	Object.defineProperty(this, 'ascent', {value: TTF_FontAscent(this.ttf)})
	Object.defineProperty(this, 'descent', {value: TTF_FontDescent(this.ttf)})
	Object.defineProperty(this, 'lineSkip', {value: TTF_FontLineSkip(this.ttf)})
	Object.defineProperty(this, 'glyphs', {value: {[32]: new Glyph(this, 32)}})
}
let RichText = exports.RichText = function RichText(str, fonts, paddingW = 0, paddingH = 0, indent = 4, editable = false, focusStyle = {border: new Border(SDL_rgba(0, 191, 255), 2)}) {
	this.str = str
	this.fonts = fonts instanceof Font ? {0: fonts} : fonts
	this.paddingW = paddingW
	this.paddingH = paddingH
	this.indent = indent
	this.editable = editable
	this.focused = false
	this.focusStyle = focusStyle
	this._focusStyle = {}
	Object.defineProperty(this, 'dstrect', {value: SDL_Rect()})
	Object.defineProperty(this, 'texture', {value: null, writable: true})
}
RichText.prototype.addStr = function(s) { this.str += s; return this }



/**
 * SDL2/GLES2 binding
 */
SDL_Init(SDL_INIT_VIDEO)
SDL_SetHint('SDL_RENDER_SCALE_QUALITY', '1')
SDL_StartTextInput()
IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG)
TTF_Init()

const window = SDL_CreateWindow('Vector painter', SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0)
let renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED)

let view = Entity({ name: 'view', bounds: new Bounds(0, 0, 1024, 768), depth: 0, origin: new Coordinates() })
let pointer = Entity({ name: 'mouse', cursorPosition: new Coordinates(), buttons: 0 })
let keyboard = Entity({ name: 'keyboard', keyStates: new Set(), focus: null })
let event = new SDL_Event()
setInterval(function() {
	while (SDL_PollEvent(event.ref())) {
		let metaType = 0
		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN: {
			let b = event.button.button - 1
			pointer.buttons |= 1 << b
			pointer.tmpPressed = b
			metaType |= POINTER_INPUT
			} break
		case SDL_MOUSEBUTTONUP: {
			let b = event.button.button - 1
			pointer.buttons &= ~(1 << b)
			pointer.tmpReleased = b
			metaType |= POINTER_INPUT
			} break
		case SDL_MOUSEMOTION: {
			let m = event.motion
			let p = pointer.cursorPosition
			pointer.tmpMotion = new Coordinates(m.xrel, m.yrel)
			p.x = m.x
			p.y = m.y
			metaType |= POINTER_INPUT
			} break
		case SDL_MOUSEWHEEL: {
			let w = event.wheel
			pointer.tmpWheel = new Coordinates(w.x, w.y)
			metaType |= POINTER_INPUT
			} break
		case SDL_KEYDOWN: {
			let c = event.key.keysym.scancode
			let s = event.key.keysym.sym
			keyboard.tmpPressed = c
			keyboard.keyStates.add(c)
			if ((s & SDLK_SCANCODE_MASK) === 0)
				keyboard.tmpSymbol = s
			metaType |= KEYBOARD_INPUT
			} break
		case SDL_KEYUP: {
			let c = event.key.keysym.scancode
			keyboard.tmpReleased = c
			keyboard.keyStates.delete(c)
			metaType |= KEYBOARD_INPUT
			} break
		case SDL_TEXTINPUT: {
			let buf = event.text.text.buffer
			keyboard.tmpText = buf.toString('utf8', 0, buf.indexOf(0))
			metaType |= KEYBOARD_INPUT
			} break
		case SDL_QUIT:
			process.exit()
		}
		if (metaType) {
			let p = pointer.cursorPosition
			let x = p.x
			let y = p.y
			MetaSystem(metaType | DISPLAY_OUTPUT)
			if (p.x !== x || p.y !== y)
				SDL_WarpMouseInWindow(window, p.x, p.y)
		}
	}
}, 25)



/**
 * Depth update - detects tree root nodes, then propagates depth order
 */
exports.DepthUpdateSystem = Entity(function DepthUpdateSystem() {
	function visit(n, depth) {
		n.depth = depth--
		if (n.children) for (let child of n.children)
			depth = visit(child, depth)
		return depth
	}
	if (TreeNodes.updated.size === 0)
		return
	let nodes = DepthUpdateSystem._nodes
	for (let n of TreeNodes) {
		for (let c of n.children) {
			if (nodes.has(c))
				throw "Node referenced twice in scene tree"
			nodes.add(c)
		}
	}
	let depth = 0
	for (let n of TreeNodes) {
		if (!nodes.has(n))
			depth = visit(n, depth)
	}
	nodes.clear()
}, { runOn: POINTER_INPUT, order: 0, _nodes: new Set() })



/**
 * Pointer target - for each Pointer, assigns target to be the topmost
 *   targetable Entity that contains the cursor
 */
exports.PointerTargetSystem = Entity(function PointerTargetSystem() {
	function pick(x, y, sel) {
		for (let e of sel) {
			let b = e.bounds
			if (x >= b.x && x < b.x + b.w && y >= b.y && y < b.y + b.h)
				return e
		}
	}
	for (let p of Pointers) {
		let x = p.cursorPosition.x
		let y = p.cursorPosition.y
		let v = pick(x, y, Views)
		let _v = p.view
		if (v !== _v)
			p.view = v
		if (!v)
			continue
		
		x -= v.bounds.x - v.origin.x
		y -= v.bounds.y - v.origin.y
		let t = pick(x, y, Targetables)
		let _t = p.target
		if (t !== _t)
			p.target = t
	}
}, { runOn: POINTER_INPUT, order: 1 })



/**
 * Shortcuts - detects and signals key presses while holding CTRL/CMD key
 */
exports.ShortcutSystem = Entity(function ShortcutSystem() {
	let s = keyboard.tmpSymbol
	if (s && keyboard.keyStates.has(process.platform == 'darwin' ? SDL_SCANCODE_LGUI : SDL_SCANCODE_LCTRL))
		keyboard.tmpShortcut = s
}, { runOn: KEYBOARD_INPUT, order: 20 })



/**
 * Pointer click - generates a click when pressing&releasing button1 on
 *   the same target and not too far apart
 */
exports.PointerClickSystem = Entity(function PointerClickSystem() {
	for (let p of Pointers) {
		let pos = p.cursorPosition
		let t = p.target
		if (p.tmpPressed === 0) {
			p.pressPosition = new Coordinates(pos.x, pos.y)
			p.pressTarget = t
		} else if (p.tmpReleased === 0 && t && t.clickable && t === p.pressTarget &&
			pos.distance(p.pressPosition) < 10) {
			p.tmpClick = t
			if (t !== undefined)
				t.tmpClickBy = p
		}
	}
}, { runOn: POINTER_INPUT, order: 21 })



/**
 * Pointer drag - implements drag&drop interaction technique
 */
exports.PointerDragSystem = Entity(function PointerDragSystem() {
	for (let p of Pointers) {
		let pos = p.cursorPosition
		let t = p.pressTarget
		let d = p.dragging // Entity being dragged
		if (!d && p.buttons & 1 && t && t.draggable && pos.distance(p.pressPosition) > 10) {
			p.dragging = d = t
			d.draggedBy = p
			delete d.targetable // cannot be "seen" by any other pointer
		}
		if (d) {
			d.bounds = d.bounds.setX(pos.x).setY(pos.y) // signal Component update to Polyphony
			if (p.tmpReleased === 0) {
				delete p.dragging
				delete d.draggedBy
				p.tmpDropped = d
				d.targetable = true
				if (p.target)
					p.target.tmpDropOf = d
			}
		}
	}
}, { runOn: POINTER_INPUT, order: 22 })



/**
 * Toggle buttons - detects clicks on Togglable buttons, and untoggles other
 *   buttons with the same toggleGroup
 */
exports.ToggleSystem = Entity(function ToggleSystem() {
	for (let p of Pointers) {
		let t = p.tmpClick
		if (!t || !Togglables.accept(t))
			continue
		let g = t.toggleGroup
		if (g !== null) for (let u of Togglables) {
			if (u.toggled && u.toggleGroup === g) {
				delete u.toggled
				let old = u._toggleStyle
				for (let c in old) {
					if (old[c] === undefined)
						delete u[c]
					else
						u[c] = old[c]
				}
				break
			}
		}
		t.toggled = true
		let style = t.toggleStyle
		let old = t._toggleStyle || (t._toggleStyle = {})
		for (let c in style) {
			old[c] = t[c]
			t[c] = style[c]
		}
	}
}, { runOn: POINTER_INPUT, order: 40 })



/**
 * Text editing - manages keyboard focus, detects key presses and updates each
 *   focused Entity's richText Component
 */
exports.TextEditSystem = Entity(function TextEditSystem() {
	// detect mouse presses on a focusable Entity
	let f = undefined
	for (let p of Pointers) {
		let t = p.target
		if (p.tmpPressed !== undefined && t && t.richText && t.richText.editable)
			f = t
	}
	
	if (f) {
		// for each keyboard, unfocus the previously focused Entity
		for (let k of Keyboards) {
			let _f = k.focus
			k.focus = f
			if (!_f)
				continue
			let _t = _f.richText
			_t.focused = false
			let _s = _t._focusStyle
			for (let c in _s) {
				if (_s[c] === undefined)
					delete _f[c]
				else
					_f[c] = _s[c]
			}
		}
		
		// make the new Entity visually focused
		let t = f.richText
		t.focused = true
		let s = t.focusStyle
		let _s = t._focusStyle || (t._focusStyle = {})
		for (let c in s) {
			_s[c] = f[c]
			f[c] = s[c]
		}
	}
	
	// send typed characters to each currently focused Entity
	for (let k of Keyboards) {
		let t = k.tmpText
		let f = k.focus
		if (t && f)
			f.richText = f.richText.addStr(t)
	}
}, { runOn: KEYBOARD_INPUT | POINTER_INPUT, order: 41 })



/**
 * Software renderer - draws filled shapes, borders, and icons of Entities
 *   in painting order
 */
let SHAPE_RECTANGLE = exports.SHAPE_RECTANGLE = 0
let SHAPE_ELLIPSE = exports.SHAPE_ELLIPSE = 1
exports.SoftRenderSystem = Entity(function SoftRenderSystem() {
	for (let e of Drawables.updated) {
		// set image position
		let bounds = e.bounds
		let image = e.image
		if (image) {
			image.dstrect.x = bounds.x + image.paddingW
			image.dstrect.y = bounds.y + image.paddingH
			let w = bounds.w - image.paddingW * 2
			let h = bounds.h - image.paddingH * 2
			image.dstrect.w = Math.min(w, image.w * h / image.h)
			image.dstrect.h = Math.min(h, image.h * w / image.w)
		}
		
		// render and position text
		let text = e.richText
		if (text) {
			let fonts = text.fonts
			let str = text.str
			let w = bounds.w - text.paddingW * 2
			let h = bounds.h - text.paddingH * 2
			let s = SDL_CreateRGBSurfaceWithFormat(0, w, h, 32, SDL_PIXELFORMAT_RGBA32)
			for (let i = 0, x = 0, y = 0, font, c; ; i++) {
				// predict the advance at the end of this word
				let xPred = x
				for (let j = i, f = font; j < str.length && (c = str.codePointAt(j)) !== 32 && c !== 10 && c !== 9; j++) {
					if (j in fonts)
						f = fonts[j]
					let g = f.glyphs[c]
					if (g === undefined)
						f.glyphs[c] = g = new Glyph(f, c)
					xPred += g.advance
				}
				
				// render the word
				for (; i < str.length && (c = str.codePointAt(i)) !== 32 && c !== 10 && c !== 9; i++) {
					if (i in fonts)
						font = fonts[i]
					let g = font.glyphs[c]
					text.dstrect.x = x
					text.dstrect.y = y
					SDL_BlitSurface(g.surface, null, s, text.dstrect.ref())
					x += g.advance
				}
				
				// advance the separator
				if (i === str.length)
					break
				if (i in fonts)
					font = fonts[i]
				if (c === 32) {
					x += font.glyphs[32].advance
				} else if (c === 10) {
					x = 0
					y += font.lineSkip
				} else if (c === 9) {
					x += font.glyphs[32].advance * text.indent
				}
			}
			
			// prepare the string for display
			text.dstrect.x = bounds.x + text.paddingW
			text.dstrect.y = bounds.y + text.paddingH
			text.dstrect.w = w
			text.dstrect.h = h
			if (text.texture)
				SDL_DestroyTexture(text.texture)
			text.texture = SDL_CreateTextureFromSurface(renderer, s)
			SDL_FreeSurface(s)
		}
	}
	
	SDL_SetRenderDrawColor(renderer, 192, 192, 192, 255)
	SDL_RenderClear(renderer)
	for (let e of Drawables) {
		let bounds = e.bounds
		let shape = e.shape
		
		// filled shapes
		let bg = e.backgroundColor
		if (bg && shape === SHAPE_RECTANGLE)
			GFX.boxColor(renderer, bounds.x, bounds.y, bounds.x + bounds.w - 1, bounds.y + bounds.h - 1, bg)
		if (bg && shape === SHAPE_ELLIPSE)
			GFX.filledEllipseColor(renderer, bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1, bg)
		
		// images
		let image = e.image
		if (image)
			SDL_RenderCopy(renderer, image.texture, null, image.dstrect.ref())
		
		// stroked shapes
		let bd = e.border
		if (bd && shape === SHAPE_RECTANGLE) {
			for (let i = -(bd.w >> 1); i < (bd.w + 1) >> 1; i++)
				GFX.rectangleColor(renderer, bounds.x + i, bounds.y + i, bounds.x + bounds.w - i, bounds.y + bounds.h - i, bd.color)
		}
		if (bd && shape === SHAPE_ELLIPSE) {
			for (let i = -(bd.w >> 1); i < (bd.w + 1) >> 1; i++)
				GFX.aaellipseColor(renderer, bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), (bounds.w >> 1) - i, (bounds.h >> 1) - i, bd.color)
		}
		
		// text
		let text = e.richText
		if (text)
			SDL_RenderCopy(renderer, text.texture, null, text.dstrect.ref())
	}
	SDL_RenderPresent(renderer)
}, { runOn: DISPLAY_OUTPUT, order: 80 })



/**
 * Delete temporary Components - clears any Components with tmp prefix, after
 *   all Systems have executed
 */
exports.DeleteTmpSystem = Entity(function DeleteTmpSystem() {
	for (let e of Entity) {
		for (let prop in e) {
			if (prop.startsWith('tmp'))
				delete e[prop]
		}
	}
}, { runOn: ALL_EVENTS, order: 100 })



/**
 * Factories - take an existing Entity (or make a new one), and augment it
 *   to acquire a given behaviour
 */
exports.Button = function(imgOrTxt, x, y, e = {}) {
	e.depth = e.depth || 0
	e.bounds = e.bounds || new Bounds(x, y, imgOrTxt.w + 8, imgOrTxt.h + 8)
	e.shape = e.shape || SHAPE_RECTANGLE
	e.backgroundColor = e.backgroundColor || SDL_rgba(240, 248, 255)
	e.targetable = true
	e.clickable = true
	if (imgOrTxt instanceof Image) {
		e.image = imgOrTxt
		imgOrTxt.paddingW = imgOrTxt.paddingH = 4
	} else {
		e.richText = imgOrTxt
	}
	return Entity(e)
}
exports.Canvas = function(x, y, w, h, e = {}) {
	e.depth = e.depth || 0
	e.bounds = new Bounds(x, y, w, h)
	e.shape = e.shape || SHAPE_RECTANGLE
	e.backgroundColor = e.backgroundColor || SDL_rgba(255, 255, 255)
	e.targetable = true
	e.children = e.children || []
	return Entity(e)
}
let avenir12 = new Font('/System/Library/Fonts/Avenir.ttc', 12, 0, SDL_rgba(0, 0, 0))
exports.Shape = function(type, w, h, e = {}) {
	function rnd(a) { return Math.floor(Math.random() * a) }
	e.depth = e.depth || 0
	e.bounds = new Bounds(0, 0, w, h)
	e.shape = type
	e.backgroundColor = e.backgroundColor || SDL_rgba(128 + rnd(128), 128 + rnd(128), 128 + rnd(128))
	e.border = e.border || new Border(SDL_rgba(0, 0, 0), 2)
	e.targetable = e.targetable || true
	e.draggable = e.draggable || true
	e.richText = e.richText || new RichText('', avenir12, 10, 10, 4, true)
	return Entity(e)
}

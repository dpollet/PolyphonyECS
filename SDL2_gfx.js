'use strict'

const SDL = require('./SDL2')
const ffi = require('ffi')
const ref = require('ref')
const Struct = require('ref-struct')

exports.FPS_UPPER_LIMIT = 200
exports.FPS_LOWER_LIMIT = 1
exports.FPS_DEFAULT = 30
exports.SDL2_GFXPRIMITIVES_MAJOR = 1
exports.SDL2_GFXPRIMITIVES_MINOR = 0
exports.SDL2_GFXPRIMITIVES_MICRO = 4
exports.SMOOTHING_OFF = 0
exports.SMOOTHING_ON = 1

const FPSmanager = exports.FPSmanager = Struct({
	framecount: 'uint32',
	rateticks: 'float',
	baseticks: 'uint32',
	lastticks: 'uint32',
	rate: 'uint32',
})
const FPSmanagerPtr = ref.refType(FPSmanager)

ffi.Library(process.platform == 'win32' ? 'SDL2_gfx' : 'libSDL2_gfx', {
	// SDL2_framerate.h
	SDL_initFramerate: [ 'void', [ FPSmanagerPtr ] ],
	SDL_setFramerate: [ 'int', [ FPSmanagerPtr, 'uint32' ] ],
	SDL_getFramerate: [ 'int', [ FPSmanagerPtr ] ],
	SDL_getFramecount: [ 'int', [ FPSmanagerPtr ] ],
	SDL_framerateDelay: [ 'uint32', [ FPSmanagerPtr ] ],
	
	// SDL2_gfxPrimitives.h
	pixelColor: [ 'int', [ 'void*', 'int16', 'int16', 'uint32' ] ],
	pixelRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	hlineColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint32' ] ],
	hlineRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	vlineColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint32' ] ],
	vlineRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	rectangleColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	rectangleRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	roundedRectangleColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	roundedRectangleRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	boxColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	boxRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	roundedBoxColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	roundedBoxRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	lineColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	lineRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	aalineColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	aalineRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	thickLineColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint32' ] ],
	thickLineRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	circleColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint32' ] ],
	circleRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	arcColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	arcRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	aacircleColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint32' ] ],
	aacircleRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	filledCircleColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint32' ] ],
	filledCircleRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	ellipseColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	ellipseRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	aaellipseColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	aaellipseRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	filledEllipseColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	filledEllipseRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	pieColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	pieRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	filledPieColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	filledPieRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	trigonColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	trigonRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	aatrigonColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	aatrigonRGBA: [ 'int', [ 'void*',  'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	filledTrigonColor: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint32' ] ],
	filledTrigonRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'int16', 'int16', 'int16', 'int16', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	polygonColor: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint32' ] ],
	polygonRGBA: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	aapolygonColor: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint32' ] ],
	aapolygonRGBA: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	filledPolygonColor: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint32' ] ],
	filledPolygonRGBA: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	texturedPolygon: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', SDL.SDL_SurfacePtr, 'int', 'int' ] ],
	bezierColor: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'int', 'uint32' ] ],
	bezierRGBA: [ 'int', [ 'void*', 'int16*', 'int16*', 'int', 'int', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	gfxPrimitivesSetFont: [ 'void', [ 'void*', 'uint32', 'uint32' ] ],
	gfxPrimitivesSetFontRotation: [ 'void', [ 'uint32' ] ],
	characterColor: [ 'int', [ 'void*', 'int16', 'int16', 'char', 'uint32' ] ],
	characterRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'char', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	stringColor: [ 'int', [ 'void*', 'int16', 'int16', 'char*', 'uint32' ] ],
	stringRGBA: [ 'int', [ 'void*', 'int16', 'int16', 'char*', 'uint8', 'uint8', 'uint8', 'uint8' ] ],
	
	// SDL2_imageFilter.h
	SDL_imageFilterMMXdetect: [ 'int', [ ] ],
	SDL_imageFilterMMXoff: [ 'void', [ ] ],
	SDL_imageFilterMMXon: [ 'void', [ ] ],
	SDL_imageFilterAdd: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterMean: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterSub: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterAbsDiff: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterMult: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterMultNor: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterMultDivby2: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterMultDivby4: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterBitAnd: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterBitOr: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterDiv: [ 'int', [ 'uchar*', 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterBitNegation: [ 'int', [ 'uchar*', 'uchar*', 'uint' ] ],
	SDL_imageFilterAddByte: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterAddUint: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uint' ] ],
	SDL_imageFilterAddByteToHalf: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterSubByte: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterSubUint: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uint' ] ],
	SDL_imageFilterShiftRight: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterShiftRightUint: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterMultByByte: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterShiftRightAndMultByByte: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar', 'uchar' ] ],
	SDL_imageFilterShiftLeftByte: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterShiftLeftUint: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterShiftLeft: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterBinarizeUsingThreshold: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar' ] ],
	SDL_imageFilterClipToRange: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'uchar', 'uchar' ] ],
	SDL_imageFilterNormalizeLinear: [ 'int', [ 'uchar*', 'uchar*', 'uint', 'int', 'int', 'int', 'int' ] ],
	
	// SDL2_rotozoom.h
	rotozoomSurface: [ SDL.SDL_SurfacePtr, [ SDL.SDL_SurfacePtr, 'double', 'double', 'int' ] ],
	rotozoomSurfaceXY: [ SDL.SDL_SurfacePtr, [ SDL.SDL_SurfacePtr, 'double', 'double', 'double', 'int' ] ],
	rotozoomSurfaceSize: [ 'void', [ 'int', 'int', 'double', 'double', 'int*', 'int*' ] ],
	rotozoomSurfaceSizeXY : [ 'void', [ 'int', 'int', 'double', 'double', 'double', 'int*', 'int*' ] ],
	zoomSurface: [ SDL.SDL_SurfacePtr, [ SDL.SDL_SurfacePtr, 'double', 'double', 'int' ] ],
	zoomSurfaceSize: [ 'void', [ 'int', 'int', 'double', 'double', 'int*', 'int*' ] ],
	shrinkSurface: [ SDL.SDL_SurfacePtr, [ SDL.SDL_SurfacePtr, 'int', 'int' ] ],
	rotateSurface90Degrees: [ SDL.SDL_SurfacePtr, [ SDL.SDL_SurfacePtr, 'int' ] ],
}, exports)

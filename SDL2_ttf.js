'use strict'

const SDL = require('./SDL2')
const ffi = require('ffi')

exports.TTF_MAJOR_VERSION = exports.SDL_TTF_MAJOR_VERSION = 2
exports.TTF_MINOR_VERSION = exports.SDL_TTF_MINOR_VERSION = 0
exports.TTF_PATCHLEVEL = exports.SDL_TTF_PATCHLEVEL = 15
exports.SDL_TTF_COMPILEDVERSION = 2015
exports.UNICODE_BOM_NATIVE = 0xFEFF
exports.UNICODE_BOM_SWAPPED = 0xFFFE
exports.TTF_STYLE_NORMAL = 0x00
exports.TTF_STYLE_BOLD = 0x01
exports.TTF_STYLE_ITALIC = 0x02
exports.TTF_STYLE_UNDERLINE = 0x04
exports.TTF_STYLE_STRIKETHROUGH = 0x08
exports.TTF_HINTING_NORMAL = 0
exports.TTF_HINTING_LIGHT = 1
exports.TTF_HINTING_MONO = 2
exports.TTF_HINTING_NONE = 3

exports.SDL_TTF_VERSION = exports.TTF_VERSION = (X) => {
	X.major = exports.SDL_TTF_MAJOR_VERSION
	X.minor = exports.SDL_TTF_MINOR_VERSION
	X.patch = exports.SDL_TTF_PATCHLEVEL
}
exports.SDL_TTF_VERSION_ATLEAST = (X, Y, Z) => exports.SDL_TTF_COMPILEDVERSION >= X * 1000 + Y * 100 + Z

ffi.Library(process.platform == 'win32' ? 'SDL2_ttf' : 'libSDL2_ttf', {
	TTF_Linked_Version: [ SDL.SDL_versionPtr, [ ] ],
	TTF_ByteSwappedUNICODE: [ 'void', [ 'int' ] ],
	TTF_Init: [ 'int', [ ] ],
	TTF_OpenFont: [ 'void*', [ 'string', 'int' ] ],
	TTF_OpenFontIndex: [ 'void*', [ 'string', 'int', 'long' ] ],
	TTF_OpenFontRW: [ 'void*', [ SDL.SDL_RWopsPtr, 'int', 'int' ] ],
	TTF_OpenFontIndexRW: [ 'void*', [ SDL.SDL_RWopsPtr, 'int', 'int', 'long' ] ],
	TTF_GetFontStyle: [ 'int', [ 'void*' ] ],
	TTF_SetFontStyle: [ 'void', [ 'void*', 'int' ] ],
	TTF_GetFontOutline: [ 'int', [ 'void*' ] ],
	TTF_SetFontOutline: [ 'void', [ 'void*', 'int' ] ],
	TTF_GetFontHinting: [ 'int', [ 'void*' ] ],
	TTF_SetFontHinting: [ 'void', [ 'void*', 'int' ] ],
	TTF_FontHeight: [ 'int', [ 'void*' ] ],
	TTF_FontAscent: [ 'int', [ 'void*' ] ],
	TTF_FontDescent: [ 'int', [ 'void*' ] ],
	TTF_FontLineSkip: [ 'int', [ 'void*' ] ],
	TTF_GetFontKerning: [ 'int', [ 'void*' ] ],
	TTF_SetFontKerning: [ 'void', [ 'void*', 'int' ] ],
	TTF_FontFaces: [ 'long', [ 'void*' ] ],
	TTF_FontFaceIsFixedWidth: [ 'int', [ 'void*' ] ],
	TTF_FontFaceFamilyName: [ 'string', [ 'void*' ] ],
	TTF_FontFaceStyleName: [ 'string', [ 'void*' ] ],
	TTF_GlyphIsProvided: [ 'int', [ 'void*', 'uint16' ] ],
	TTF_GlyphMetrics: [ 'int', [ 'void*', 'uint16', 'int*', 'int*', 'int*', 'int*', 'int*' ] ],
	TTF_SizeText: [ 'int', [ 'void*', 'string', 'int*', 'int*' ] ],
	TTF_SizeUTF8: [ 'int', [ 'void*', 'string', 'int*', 'int*' ] ],
	TTF_SizeUNICODE: [ 'int', [ 'void*', 'uint16*', 'int*', 'int*' ] ],
	TTF_RenderText_Solid: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32' ] ],
	TTF_RenderUTF8_Solid: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32' ] ],
	TTF_RenderUNICODE_Solid: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16*', 'uint32' ] ],
	TTF_RenderGlyph_Solid: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16', 'uint32' ] ],
	TTF_RenderText_Shaded: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32', 'uint32' ] ],
	TTF_RenderUTF8_Shaded: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32', 'uint32' ] ],
	TTF_RenderUNICODE_Shaded: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16*', 'uint32', 'uint32' ] ],
	TTF_RenderGlyph_Shaded: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16', 'uint32', 'uint32' ] ],
	TTF_RenderText_Blended: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32' ] ],
	TTF_RenderUTF8_Blended: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32' ] ],
	TTF_RenderUNICODE_Blended: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16*', 'uint32' ] ],
	TTF_RenderText_Blended_Wrapped: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32', 'uint32' ] ],
	TTF_RenderUTF8_Blended_Wrapped: [ SDL.SDL_SurfacePtr, [ 'void*', 'string', 'uint32', 'uint32' ] ],
	TTF_RenderUNICODE_Blended_Wrapped: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16*', 'uint32', 'uint32' ] ],
	TTF_RenderGlyph_Blended: [ SDL.SDL_SurfacePtr, [ 'void*', 'uint16', 'uint32' ] ],
	TTF_CloseFont: [ 'void', [ 'void*' ] ],
	TTF_Quit: [ 'void', [ ] ],
	TTF_WasInit: [ 'int', [ ] ],
	TTF_GetFontKerningSize: [ 'int', [ 'void*', 'int', 'int' ] ],
	TTF_GetFontKerningSizeGlyphs: [ 'int', [ 'void*', 'uint16', 'uint16' ] ],
}, exports)

exports.TTF_RenderText = exports.TTF_RenderText_Shaded
exports.TTF_RenderUTF8 = exports.TTF_RenderUTF8_Shaded
exports.TTF_RenderUNICODE = exports.TTF_RenderUNICODE_Shaded
exports.TTF_SetError = SDL.SDL_SetError
exports.TTF_GetError = SDL.SDL_GetError

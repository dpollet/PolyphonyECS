Polyphony ECS toolkit
=====================

Polyphony is an experimental toolkit demonstrating the use of Entity-Component-System (ECS) to design Graphical User Interfaces (GUI). It also extends the original ECS model to support advanced interfaces.

Installation
------------

Polyphony requires [Node.js](https://nodejs.org/) (we recommend LTS, packages break sometimes with latest version) and [SDL2](https://www.libsdl.org/download-2.0.php) (including SDL2_gfx, SDL2_image, and SDL2_ttf).

```
git clone https://gitlab.inria.fr/Loki/PolyphonyECS.git
cd PolyphonyECS
npm i ffi ref ref-struct ref-union ref-array
node Painter.js
```

ECS extensions
--------------

* Systems are entities → we can model their dependencies and ordering with components
* Devices are also entities → it makes it easy to access their data, and allows the support for multiple devices (mice, keyboards, ...)
* Events are signaled using *temporary* components (deleted at the end of the systems chain) → systems can react to events without callbacks and observer patterns
